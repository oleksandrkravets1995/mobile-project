const initialState = {
	test: 0,
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case 'SET_TEST': {
			return {
				...state,
				test: action.payload,
			};
		}
		default:
			return state;
	}
};

export default reducer;
