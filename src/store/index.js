import { useSelector as useReduxSelector } from 'react-redux';
import { combineReducers } from 'redux';
// import { ThunkDispatch } from 'redux-thunk';
import test from './test/reducer';

const rootReducer = combineReducers({
	test
});

export const useSelector = useReduxSelector;

export default rootReducer;