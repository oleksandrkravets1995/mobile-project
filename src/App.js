import './App.css';
import Layout from './screens/Layout'

function App() {
  return (
    <div className="App">
      <Layout />
    </div>
  );
}

export default App;
