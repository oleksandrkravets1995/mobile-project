import { useState } from "react";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

export const HappyPage = () => {
  const [percentage, setPercentage] = useState(50);
  return (
    <div className={["page", "happy-page"].join(" ")}>
      <div className="container">
        <h2>How happy you are ?</h2>
        <div className="progressBar">
          <div>
            <CircularProgressbar
              value={percentage}
              text={`${percentage}%`}
              strokeWidth={10}
              counterClockwise={true}
              styles={buildStyles({
                // Rotation of path and trail, in number of turns (0-1)
                rotation: 0.55,

                // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
                strokeLinecap: "butt",

                // Text size
                textSize: "14px",

                // How long animation takes to go from one percentage to another, in seconds
                pathTransitionDuration: 0.5,

                // Can specify path transition in more detail, or remove it entirely
                // pathTransition: 'none',

                // Colors
                pathColor: `rgba(88 70 54)`,
                textColor: "rgba(0, 0, 0)",
                trailColor: "rgb(253, 244, 234)",
                backgroundColor: "rgb(253, 244, 234)",
                border: "1px solid red",
              })}
            />
          </div>
        </div>
        <div className="inputWrap">
          <input
            type="range"
            onChange={(e) => setPercentage(e.target.value)}
            value={percentage}
          />
        </div>
        <div className="smileWrapp">
          <div className="smile-circle">
            <div className="eye-left"></div>
            <div className="eye-right"></div>
            <div className="line"></div>
          </div>
        </div>
        <div className="buttonWrap">
          <button type="button">Share my opinions</button>
        </div>
        <div className="footer-desc">
          Thanks for these informations! They will help us <pre /> to improve
          the experience of the AI ULA festival.
        </div>
      </div>
    </div>
  );
};
