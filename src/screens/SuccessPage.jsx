export const SuccessPage = () => {
  return (
    <div className={["page", "success-page"].join(" ")}>
      <div className="container">
        <h2> Your welcome</h2>
        <div className="desciption">
          <p>
            Thank you for your time <pre /> &amp; enjoy your time at the <pre />
            festival{" "}
          </p>
        </div>

        <div className="icon-wrapper">
          <img
            src="https://icon-library.com/images/alien-icon-png/alien-icon-png-25.jpg"
            alt="festival-icon"
          />
        </div>
        <p className="footer-desc">
          <span>Al Ula Dattes festival </span> thanks you <pre /> for coming and
          for your attention.
        </p>
      </div>
    </div>
  );
};
