import React, { useState, useEffect } from 'react';
import { CSSTransition } from "react-transition-group";

export const OpinionInfo = () => {
  const [active, setActive] = useState(false);
  useEffect(() => {
    setActive(true)
    return () => {
      setActive(false)
    }
  }, [])
  return (
    <div className={["page", "opinion-info-page"].join(" ")}>
      <CSSTransition in={active} classNames="from_left"
        timeout={750} mountOnEnter unmountOnExit>
      <div className="circle">
        <p>
          Your opinion <pre /> matters
        </p>
      </div>
      </CSSTransition>
      <CSSTransition in={active} classNames="from_right"
        timeout={750} mountOnEnter unmountOnExit>
      <div className="arrow">
        <svg
          width="24px"
          height="24px"
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M6 12H18.5M18.5 12L12.5 6M18.5 12L12.5 18"
            stroke="rgba(88 70 54)"
            strokeWidth="1.5"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      </div>
      </CSSTransition>
    </div>
  );
};
