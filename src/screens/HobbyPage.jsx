export const HobbyPage = () => {
  return (
    <div className={["page", "hobby-page"].join(" ")}>
      <div className="container">
        <h2>
          What is the thing that <pre /> you loved the most?
        </h2>
        <div style={{ marginTop: "20px" }}>
          <div className="section">
            <div className="item"></div>
            <div className="item"></div>
          </div>
          <div className="section">
            <div className="item"></div>
            <div className="item"></div>
          </div>
        </div>
        <h2>
          Ideas to imrove the <pre /> festival?
        </h2>
        <div className="textareaWrap">
          <textarea
            name=""
            id=""
            cols="33"
            rows="13"
            placeholder="Text..."
          ></textarea>
        </div>
      </div>
    </div>
  );
};
