export * from './WelcomePage';
export * from './MapPage';
export * from './OpinionInfo';
export * from './SuccessPage';
export * from './HappyPage';
export * from './HobbyPage';
export * from './PersonInfoPage';