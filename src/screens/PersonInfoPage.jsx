import React, { useState, useEffect } from 'react';
import { CSSTransition } from "react-transition-group";

export const PersonInfoPage = () => {
  const [age, setAge] = useState(12);
  const [active, setActive] = useState(false);
  useEffect(() => {
    setActive(true)
    return () => {
      setActive(false)
    }
  }, [])
  return (
    <div className={["page", "personInfo-page"].join(" ")}>
      <div className="container">
      <CSSTransition in={active} classNames="from_left"
        timeout={750} mountOnEnter unmountOnExit>
        <h2>
          Fews Questions <pre />
          About you
        </h2>
      </CSSTransition>
      <CSSTransition in={active} classNames="from_right"
        timeout={750} mountOnEnter unmountOnExit>
          <>
        <h2 style={{ marginTop: "60px" }}>How old are you ?</h2>
        <div style={{ textAlign: "center" }} className="inputWrap">
          <input
            type="range"
            min={0}
            max={100}
            onChange={(e) => setAge(e.target.value)}
            value={age}
          />
        </div>
        <h3 style={{ color: "#cb9969", textAlign: "center" }}>{age} ans</h3>
        </>
      </CSSTransition>

        <h2 style={{ marginTop: "60px" }}>
          {" "}
          Have you been here <pre />
          before?
        </h2>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            gap: "10px",
            width: "100%",
          }}
        >
          <div className="section-item">Qui</div>
          <div className="section-item">Non</div>
        </div>
      </div>
    </div>
  );
};
