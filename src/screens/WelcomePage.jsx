import { useDispatch } from 'react-redux';
import React, {useEffect} from 'react';

export const WelcomePage = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({type: 'SET_TEST', payload: 2})
  }, [dispatch])
  return (
    <div className={["page", "welcome-page"].join(" ")}>
      <h2>Hello !</h2>
      <p>
        The Royal Commision for Alula is happy to welcome you to the{" "}
        <span>2021 date festival</span>
      </p>
      <h4>
        Swipe up{" "}
        <span>
          <svg
            width="24px"
            height="24px"
            viewBox="0 0 24 24"
            id="arrow_down"
            data-name="arrow down"
            xmlns="http://www.w3.org/2000/svg"
          >
            <defs>
              <clipPath id="clipPath">
                <rect
                  id="Mask"
                  width="24"
                  height="24"
                  fill="none"
                  stroke="#edb672"
                  strokeWidth="1"
                />
              </clipPath>
            </defs>
            <g
              id="_20x20_arrow-back--grey"
              data-name="20x20/arrow-back--grey"
              transform="translate(0 24) rotate(-90)"
            >
              <rect
                id="Mask-2"
                data-name="Mask"
                width="24"
                height="24"
                fill="none"
              />
              <g
                id="_20x20_arrow-back--grey-2"
                data-name="20x20/arrow-back--grey"
                clipPath="url(#clipPath)"
              >
                <g id="Group_2" data-name="Group 2" transform="translate(8 7)">
                  <path
                    id="Rectangle"
                    d="M0,6.586V0H6.586"
                    transform="translate(0.686 5.157) rotate(-45)"
                    fill="none"
                    stroke="#edb672"
                    strokeMiterlimit="10"
                    strokeWidth="1.5"
                  />
                </g>
              </g>
            </g>
          </svg>
        </span>
      </h4>
    </div>
  );
};
