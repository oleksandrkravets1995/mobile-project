import React, { useState, useEffect } from 'react';

import { CSSTransition } from "react-transition-group";

export const MapPage = () => {
  const [active, setActive] = useState(false);
  useEffect(() => {
    setActive(true)
    return () => {
      setActive(false)
    }
  }, [])

  return (
    <div className={["page", "map-page"].join(" ")}>
      <div className="container">
        <CSSTransition in={active} classNames="from_left"
        timeout={300}  mountOnEnter unmountOnExit>
        <h2>
          Where is it located <pre />
          on the map?
        </h2>
        </CSSTransition>
        <CSSTransition  in={active} classNames="from_right" timeout={300} >
        <div className="map-wrapper"></div>
        </CSSTransition>
      </div>
    </div>
  );
};
