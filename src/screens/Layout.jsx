import { useSwipeable } from 'react-swipeable';
import React, { useState } from 'react';
import { WelcomePage, MapPage, OpinionInfo, SuccessPage, HappyPage, HobbyPage, PersonInfoPage } from './index';


const Layout = () => {
    const [componentNum, setComponentNum] = useState(0);
    const [animationDirectionClass, setAnimationDirectionState] = useState('');
    const swipe = useSwipeable({
        onSwipedUp: () => {
          setAnimationDirectionState('forward');
          componentNum !== 6 && setComponentNum(componentNum + 1)
        },
        onSwipedDown: () => {
          setAnimationDirectionState('back');
          componentNum && setComponentNum(componentNum - 1)
        },
      });
      
    const renderSwitch = (param) => {
        switch(param) {
          case 0:
            return <WelcomePage />;
          case 1:
            return <MapPage />;
          case 2:
            return <OpinionInfo />;
          case 3:
            return <PersonInfoPage />;
          case 4:
            return <HobbyPage />;
          case 5:
            return <HappyPage />;
          case 6:
            return <SuccessPage />;
          default:
            return <WelcomePage />;
        }
      }
      return (
        <div {...swipe} className={["App", animationDirectionClass].join(" ")}>
          {renderSwitch(componentNum)}
        </div>
      );
  };
  
export default Layout;